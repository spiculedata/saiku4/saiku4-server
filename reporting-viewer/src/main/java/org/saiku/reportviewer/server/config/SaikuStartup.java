package org.saiku.reportviewer.server.config;

import org.saiku.repository.ScopedRepo;
import org.saiku.rpc.Protocol;
import org.saiku.rpc.consumers.*;
import org.saiku.service.util.security.authorisation.MustBeAuthenticatedAuthorisation;
import org.saiku.web.service.SessionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Component;

@Component
public class SaikuStartup implements InitializingBean {
    @Autowired
    Protocol rpcProtocol;
    @Autowired
    private EmailAndPasswordAuthConsumer emailAndPasswordAuthConsumer;
    @Autowired
    private CookieAuthConsumer cookieAuthConsumer;
    @Autowired
    private CasHeaderAuthConsumer casHeaderAuthConsumer;
    @Autowired
    private GenerateReportConsumer generateReportConsumer;
    @Autowired
    private CreateDatasourceConsumer createDatasourceConsumer;

    @Autowired
    private AuthenticationManager auth;

    @Override
    public void afterPropertiesSet() throws Exception {
        rpcProtocol.registerMessage("email_password_auth", emailAndPasswordAuthConsumer);
        rpcProtocol.registerMessage("cookie_auth",         cookieAuthConsumer);
        rpcProtocol.registerMessage("cas_header_auth",     casHeaderAuthConsumer);
        rpcProtocol.registerMessage("generate_report",     generateReportConsumer);
        rpcProtocol.registerMessage("create_datasource",   createDatasourceConsumer);

        System.out.println(" [x] Saiku Ready - Awaiting RPC requests");
        rpcProtocol.buildAndStart();
    }

    private void initSessionService() throws Exception {
        ScopedRepo sessionRepo = new ScopedRepo();
        MustBeAuthenticatedAuthorisation authorisationPredicate = new MustBeAuthenticatedAuthorisation();

        // bi.meteorite.LicenseUtils licenseUtils = new bi.meteorite.LicenseUtils();
        //licenseUtils.init();

        SessionService sessionService = new SessionService();
        sessionService.setSessionRepo(sessionRepo);
        sessionService.setAuthorisationPredicate(authorisationPredicate);
        sessionService.setAuthenticationManager(auth);
    }
}
