package org.saiku.session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class SaikuSessionManager {
  public static final long SESSION_TIMEOUT = 5 * 60 * 1000L; // 5 minutes
  public static final long ONE_SECOND = 1000L; // 1 second
  
  private static SaikuSessionManager instance;
  
  private boolean running;
  private Map<String, SaikuSession> activeSessions;
  
  private SaikuSessionManager() {
    this.activeSessions = new HashMap<>();
    this.startSessionCleaupThread();
  }
  
  public static SaikuSessionManager getInstance() {
    if (instance == null) {
      instance = new SaikuSessionManager();
    }
    
    return instance;
  }
  
  public void stop() {
    this.running = false;
  }
  
  protected void startSessionCleaupThread() {
    this.running = true;
    new Thread() {
      @Override
      public void run() {
        while(running) {
          synchronized(activeSessions) {
            List<String> timedOutSessions = new ArrayList<>();
            
            // Discovering which sessions are inactive for more than a timeout threshold
            for (String sessionId : activeSessions.keySet()) {
              SaikuSession session = activeSessions.get(sessionId);
              if (System.currentTimeMillis() - session.getUpdatedAt() > SESSION_TIMEOUT) {
                timedOutSessions.add(sessionId);
              }
            }
            
            // Removing the timed out sessions
            for (String sessionId : timedOutSessions) {
              activeSessions.remove(sessionId);
            }
          }
          
          try {
            Thread.sleep(ONE_SECOND);
          } catch (InterruptedException ex) {
            // Do nothing - it's excepted
          }
        }
      }
    }.start();
  }
  
  public SaikuSession createSessionWithEmailAndPassword(String email, String password) {
    String sessionId = UUID.randomUUID().toString();
    SaikuSession session = new SaikuSession(sessionId);
    this.registerSession(sessionId, session);
    return session;
  }
  
  protected void registerSession(String sessionId, SaikuSession session) {
    this.activeSessions.put(sessionId, session);
  }
  
  public SaikuSession getSession(String sessionId) {
    return this.activeSessions.get(sessionId);
  }
}
