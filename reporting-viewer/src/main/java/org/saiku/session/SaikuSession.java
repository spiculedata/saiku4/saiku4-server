package org.saiku.session;

import java.io.Serializable;

public class SaikuSession implements Serializable {
  private static final long serialVersionUID = 3896910487046500996L;

  private String sessionId;
  private long updatedAt;
  
  public SaikuSession(String sessionId) {
    this.sessionId = sessionId;
    this.updateTime();
  }
  
  public long getUpdatedAt() {
    return this.updatedAt;
  }
  
  private void updateTime() {
    this.updatedAt = System.currentTimeMillis();
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SaikuSession other = (SaikuSession) obj;
    if (sessionId == null) {
      if (other.sessionId != null)
        return false;
    } else if (!sessionId.equals(other.sessionId))
      return false;
    return true;
  }
}
