package org.saiku.rpc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

import org.springframework.stereotype.Component;

@Component
public class Protocol {
    public static final int MAX_TRIES = 10;
    public static final long DELAY_BETWEEN_TRIES = 5000; // 5 seconds

    public static final String LOCALHOST = "localhost";
    public static final String RABBITMQ_HOST = "RABBITMQ_HOST";

    private String hostname;
    private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;

    private Map<String, Consumer> messageMap;

    public Protocol() {
        this(Protocol.getRabbitMqHost());
    }

    public Protocol(String hostname) {
        this.hostname = hostname;
        this.messageMap = createMap();
        this.tryToConnect();
    }

    private void tryToConnect() {
        // Sometimes RabbitMQ server is still not available, so we gonna try
        // connecting to it several times before giving up
        for (int i = 1; i <= MAX_TRIES; i++) {
            System.out.println("Attempt " + i + " of " + MAX_TRIES + " to connect to RabbitMQ server ...");
            try {
                //Try to connect
                this.setupConnectionFactory();
                this.setupChannel();

                System.out.println("Connected !");
                //Yay, we have a connection ! So, we can stop trying
                break;
            } catch (Exception ex) {
                System.out.println("Could not connect to server, retrying in 5 seconds ...");
                try {
                    // It wasn't this time, so let's wait awhile before retrying
                    Thread.sleep(DELAY_BETWEEN_TRIES);
                } catch (InterruptedException iex) {
                    // Expected
                }

                throw new RuntimeException("Could not connect to RabbitMQ server", ex);
            }
        }
    }

    private static String getRabbitMqHost() {
        String rabbitMqHost = System.getenv(RABBITMQ_HOST);
        return rabbitMqHost != null ? rabbitMqHost : LOCALHOST;
    }

    public Channel getChannel() {
        return channel;
    }

    public void registerMessage(String message, Consumer consumer) {
        this.messageMap.put(message, consumer);
    }

    public void buildAndStart() {
        try {
            for (String message : this.messageMap.keySet()) {
                this.channel.queueDeclare(message, false, false, false, null);
                this.channel.basicConsume(message, false, this.messageMap.get(message));
            }

            // Start Waiting for Messages
            Consumer consumer = this.messageMap.get(this.messageMap.keySet().iterator().next());
            while (true) {
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException _ignore) {
                }
            }
        }
    }

    private void setupConnectionFactory() {
        this.factory = new ConnectionFactory();
        this.factory.setHost(this.hostname);
    }

    private void setupChannel() throws IOException, TimeoutException {
        this.connection = factory.newConnection();
        this.channel = connection.createChannel();
        this.channel.basicQos(1);
    }

    private Map<String, Consumer> createMap() {
        return new HashMap<>();
    }
}
