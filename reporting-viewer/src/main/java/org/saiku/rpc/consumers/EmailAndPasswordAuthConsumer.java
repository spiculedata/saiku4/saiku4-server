package org.saiku.rpc.consumers;

import java.util.Map;

import org.saiku.rpc.Protocol;
import org.saiku.rpc.Util;
import org.saiku.session.SaikuSession;
import org.saiku.session.SaikuSessionManager;
import static org.saiku.rpc.Util.entry;
import static org.saiku.rpc.Util.buildMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmailAndPasswordAuthConsumer extends SaikuDefaultConsumer {
  @Autowired
  public EmailAndPasswordAuthConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processMessage(byte[] body) throws Exception {
    Map<String, Object> fields = Util.jsonToMap(new String(body));

    SaikuSession session = SaikuSessionManager.getInstance().createSessionWithEmailAndPassword(
        (String) fields.get("email"),
        (String) fields.get("password"));

    return Util.mapToJson(buildMap(entry("sessionId", session.getSessionId()))).getBytes();
  }
}
