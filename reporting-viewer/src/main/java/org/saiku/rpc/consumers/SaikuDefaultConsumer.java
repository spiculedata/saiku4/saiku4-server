package org.saiku.rpc.consumers;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.saiku.rpc.Protocol;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class SaikuDefaultConsumer extends DefaultConsumer {
    private Protocol rpcProtocol;

    @Autowired
    public SaikuDefaultConsumer(Protocol rpcProtocol) {
        super(rpcProtocol.getChannel());
        this.rpcProtocol = rpcProtocol;
    }

    public Protocol getRpcProtocol() {
        return rpcProtocol;
    }

    public abstract byte[] processMessage(byte[] body) throws Exception;

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
            throws IOException {
        AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();

        try {
            this.getChannel().basicPublish("", properties.getReplyTo(), replyProps, this.processMessage(body));
        } catch (Exception ex) {
            System.err.println("Error sending the report bytes through queue: " + ex.getMessage());
        }

        this.getChannel().basicAck(envelope.getDeliveryTag(), false);

        // RabbitMq consumer worker thread notifies the RPC server owner thread
        synchronized (this) {
            this.notify();
        }
    }
}
