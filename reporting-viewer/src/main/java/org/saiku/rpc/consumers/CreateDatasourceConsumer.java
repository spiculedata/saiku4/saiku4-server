package org.saiku.rpc.consumers;

import com.rabbitmq.client.Channel;
import org.saiku.rpc.Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateDatasourceConsumer extends SaikuDefaultConsumer {
    @Autowired
    public CreateDatasourceConsumer(Protocol rpcProtocol) {
        super(rpcProtocol);
    }

    @Override
    public byte[] processMessage(byte[] body) throws Exception {
        return "CreateDatasourceConsumer".getBytes();
    }

}
