package org.saiku.rpc.consumers;

import java.util.Map;

import org.saiku.rpc.Protocol;
import org.saiku.rpc.Util;
import org.saiku.session.SaikuSession;
import org.saiku.session.SaikuSessionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.saiku.rpc.Util.buildMap;
import static org.saiku.rpc.Util.entry;

@Component
public class CasHeaderAuthConsumer extends SaikuDefaultConsumer {
  @Autowired
  public CasHeaderAuthConsumer(Protocol rpcProtocol) {
    super(rpcProtocol);
  }

  @Override
  public byte[] processMessage(byte[] body) throws Exception {
    Map<String, Object> fields = Util.jsonToMap(new String(body));

    SaikuSession session = SaikuSessionManager.getInstance().createSessionWithEmailAndPassword(
        (String) fields.get("cas_header"),
        (String) fields.get("cas_header"));

    return Util.mapToJson(buildMap(entry("sessionId", session.getSessionId()))).getBytes();
  }
}
