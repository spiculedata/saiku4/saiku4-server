package org.saiku.rpc.consumers;

import java.io.File;
import java.io.FileInputStream;

import org.saiku.reportviewer.server.api.ReportServerImpl;

import com.rabbitmq.client.Channel;
import org.saiku.rpc.Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenerateReportConsumer extends SaikuDefaultConsumer {
    private static ReportServerImpl server;

    @Autowired
    public GenerateReportConsumer(Protocol rpcProtocol) {
        super(rpcProtocol);

        if (server == null) {
            server = new ReportServerImpl();
            server.init();
        }
    }

    @Override
    public byte[] processMessage(byte[] body) throws Exception {
        File temp = File.createTempFile("saiku", ".pdf");
        server.processReport(temp, "test", "pdf", new org.saiku.reportviewer.server.util.MockUriInfo());

        byte[] buffer = new byte[(int) temp.length()];

        try {
            FileInputStream fis = new FileInputStream(temp);
            fis.read(buffer);
            fis.close();
        } catch (Exception ex) {
            System.err.println("Error converting the report to PDF: " + ex.getMessage());
        }

        return buffer;
    }
}
