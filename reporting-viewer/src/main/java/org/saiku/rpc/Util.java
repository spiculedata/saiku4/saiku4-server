package org.saiku.rpc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Util {
  public static Map<String, Object> jsonToMap(String json) {
    Map<String, Object> map = new HashMap<>();

    try {
      map = jsonObjToMap(new JSONObject(json));
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return map;
  }

  private static Map<String, Object> jsonObjToMap(JSONObject jsonObj) throws JSONException {
    Map<String, Object> map = new HashMap<>();

    for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ){
      String key = it.next();
      Object value = jsonObj.get(key);
      map.put(key, convertFromJson(value));
    }

    return map;
  }

  private static Object convertFromJson(Object value) throws JSONException {
    if (value instanceof JSONObject) {
      return jsonObjToMap((JSONObject) value);
    } else if (value instanceof JSONArray) {
      List<Object> list = new ArrayList<>();
      JSONArray array = (JSONArray)value;

      for (int i = 0; i < array.length(); i++) {
        list.add(convertFromJson(array.get(i)));
      }

      return list;
    }

    return value == null ? "" : value.toString();
  }

  public static String mapToJson(Map<String, Object> map) {
    JSONObject jsonObj = new JSONObject();

    for (String key : map.keySet()) {
      Object value = map.get(key);

      try {
        jsonObj.put(key, value);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }

    return jsonObj.toString();
  }

  public static <K, V> Map.Entry<K, V> entry(K key, V value) {
    return new AbstractMap.SimpleEntry<>(key, value);
  }

  public static <K, U> Collector<Map.Entry<K, U>, ?, Map<K, U>> entriesToMap() {
    return Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue());
  }

  public static <K, V> Map<K, V> buildMap(Map.Entry<K, V> ...entries) {
    return Collections.unmodifiableMap(Stream.of(entries).collect(entriesToMap()));
  }
}
