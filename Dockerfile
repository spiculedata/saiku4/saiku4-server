FROM openjdk:8

COPY reporting-viewer/target/saiku-server.jar /saiku-server.jar
ADD reporting-viewer/target/classes/lib /lib

CMD java -cp saiku-server.jar:lib/* org.saiku.reportviewer.server.Main
